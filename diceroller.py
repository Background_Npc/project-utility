import random

def roll(sides=100):
	num_rolled = random.randint(1,sides)
	return num_rolled
def main():
	sides=100
	rolling = True
	while rolling:
		print("	")
		roll_again=input("Try your Luck? ENTER=Roll. Q=Quit")
		print("	")
		if roll_again.lower()!="q":
			num_rolled=roll(sides)
			print("You rolled a",num_rolled)
		else:
			rolling= False
			print("Thanks for playing")
main()


